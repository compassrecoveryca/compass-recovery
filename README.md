Compass Recovery provides drug detox and residential addiction and dual diagnosis treatment for men and women. Specialized in severe mental health stabilization and methadone detox, we provide 100% customized treatment programs based on our clients' individual needs. Call us 24/7 for help.

Address: 3151 Airway Drive, #F105B, Costa Mesa, CA 92626, USA

Phone: 949-370-6018

Website: https://compass-recovery.com
